# Laravel(Docker)

## Getting Started

Laravel実装環境をDockerで構築   
PHP + Laravel + PostgreSQL

## Running

```
$ ./docker.sh start
$ ./docker.sh ssh
$ ./docker.sh stop
$ ./docker.sh psrm
```

start: dockerコンテナ起動   
ssh: コンテナにssh接続   
stop: コンテナ停止、プロセス履歴削除   
psrm: プロセス履歴削除   

### ディレクトリ構成

```
|- README.md
|- Dockerfile
|- docker.sh
|- work
|- db
|- lib
```

**Dockerfile**   
`$ docker build .`で環境構築するため

**docker.sh**   
上記起動、停止、接続コマンド

**work**   
ワーキングディレクトリ   
コンテナ(/work)と共有ディレクトリ設定

**db**   
Dockerコンテナ内PostgreSQLの永続化   
コンテナと共有ディレクトリ設定   

**lib**   
必要ファイル

## docker.sh
### start   
ポートフォワードでLaravel(8000), Postgres(5432)を設定   
ホストのワーキングディレクトリとdbディレクトリをDockerコンテナの任意のディレクトリに共有設定。   
共有設定することでDocker上で実装したもの、PostgreSQLのDBもホストに残る。   

/sbin/init   
Docker CentOSではsbinが起動したいため、sudo, systemctlが利用できない。   
/sbin/initを起動した状態で-dでデーモン化することで上記の利用が可能   

### stop   
デーモン化されたDockerコンテナを停止し、ps履歴を全削除
   
### ssh   
デーモン化されたDockerコンテナに接続   

## Laravel   
`$ php artisan serve --host 0.0.0.0`   
host指定をしないとlocalhost以外からアクセスできない
