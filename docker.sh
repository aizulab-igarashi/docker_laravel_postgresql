#!/bin/bash
IMAGE_NAME=""
Docker="/usr/local/bin/docker"
WorkingDirectory=""
DBDirectory=""
ContainerName=""

if [ $1 = "start" ]; then
  $Docker run --privileged -d --name $ContainerName -v $DBDirectory:/var/lib/pgsql/data -v $WorkingDirectory:/work -p 8000:8000 -p 5432:5432 $IMAGE_NAME /sbin/init
elif [ $1 = "stop" ]; then
  $Docker stop $ContainerName
  $Docker rm $(docker ps -aq)
elif [ $1 = "ssh" ]; then
  $Docker exec -it $ContainerName /bin/bash
elif [ $1 = "psrm" ]; then
  $Docker rm $(docker ps -aq)
fi
