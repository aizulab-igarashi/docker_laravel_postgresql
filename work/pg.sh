#!/bin/bash

if [ $1 = "start" ]; then
  /usr/bin/systemctl start postgresql.service
elif [ $1 = "stop" ]; then
  /usr/bin/systemctl stop postgresql.service
elif [ $1 = "status" ]; then
  /usr/bin/systemctl status postgresql.service
elif [ $1 = "restart" ]; then
  /usr/bin/systemctl restart postgresql.service
fi
